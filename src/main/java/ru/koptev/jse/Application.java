package ru.koptev.jse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.koptev.jse.process.ApplicationProcess;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("ru.koptev.jse");
        ApplicationProcess applicationProcess = (ApplicationProcess) context.getBean("applicationProcess");
        applicationProcess.process();
    }

}
