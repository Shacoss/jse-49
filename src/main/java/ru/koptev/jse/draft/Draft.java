package ru.koptev.jse.draft;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.koptev.jse.form.VisaApplicationForm;
import ru.koptev.jse.model.Form;
import ru.koptev.jse.ui.CommandInterface;

import java.io.*;

@Log4j2
@Component
public class Draft {

    private final ApplicationContext applicationContext;
    private final CommandInterface commandInterface;

    @Autowired
    public Draft(ApplicationContext applicationContext, CommandInterface commandInterface) {
        this.applicationContext = applicationContext;
        this.commandInterface = commandInterface;
    }

    public void save(Form form) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("Draft"))) {
            objectOutputStream.writeObject(form);
            commandInterface.write(form.getVisa() + " visa saved in draft");
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public VisaApplicationForm load() {
        commandInterface.write("Loading draft");
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("Draft"))) {
            Form form = (Form) objectInputStream.readObject();
            switch (form.getVisa()) {
                case "UK":
                    VisaApplicationForm visaApplicationForm = (VisaApplicationForm) applicationContext.getBean("visaUK");
                    visaApplicationForm.setForm(form);
                    commandInterface.write("Visa UK loaded successfully");
                    return visaApplicationForm;
                case "Schengen":
                    visaApplicationForm = (VisaApplicationForm) applicationContext.getBean("visaSchengen");
                    visaApplicationForm.setForm(form);
                    commandInterface.write("Visa Schengen loaded successfully");
                    return visaApplicationForm;
                default:
                    commandInterface.write("Loading error");
                    break;
            }
        } catch (IOException | ClassNotFoundException e) {
            commandInterface.write("Draft not found");
            return null;
        }
        return null;
    }

}
