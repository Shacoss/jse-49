package ru.koptev.jse.form;

import ru.koptev.jse.model.Form;

/**
 * Форма данных заявки на визу.
 */
public interface VisaApplicationForm {
    /**
     * Запрашивает и заполняет данные формы.
     */
    void fillForm();

    /**
     * Возвращает данные формы.
     */
    String getFormData();

    Form getForm();

    void setForm(Form form);
}
