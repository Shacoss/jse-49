package ru.koptev.jse.form.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.koptev.jse.draft.Draft;
import ru.koptev.jse.form.VisaApplicationForm;
import ru.koptev.jse.model.Form;
import ru.koptev.jse.ui.CommandInterface;

@Log4j2
@Component
@Scope(scopeName = "prototype")
public class VisaUK implements VisaApplicationForm {

    private static final String SAVE_DRAFT = "Save";
    private final CommandInterface commandInterface;
    private final Draft draft;
    private Form form;

    @Autowired
    public VisaUK(CommandInterface commandInterface, Draft draft) {
        this.commandInterface = commandInterface;
        this.draft = draft;
    }

    @Override
    public void fillForm() {
        if (form == null) {
            form = new Form();
        }
        try {
            form.setVisa("UK");
            String command;
            if (form.getFistName() == null) {
                command = commandInterface.writeAndRead("Enter fist name or save visa draft");
                if (SAVE_DRAFT.equals(command)) {
                    draft.save(form);
                    return;
                }
                form.setFistName(command);
            }
            if (form.getLastName() == null) {
                command = commandInterface.writeAndRead("Enter last name or save visa draft");
                if (SAVE_DRAFT.equals(command)) {
                    draft.save(form);
                    return;
                }
                form.setLastName(command);
            }
            if (form.getAge() == 0) {
                command = commandInterface.writeAndRead("Enter age or save visa draft");
                if (SAVE_DRAFT.equals(command)) {
                    draft.save(form);
                    return;
                }
                form.setAge(Integer.parseInt(command));
            }
        } catch (NumberFormatException e) {
            commandInterface.write("Incorrect age");
        }
    }

    @Override
    public String getFormData() {
        return form.toString();
    }

    @Override
    public Form getForm() {
        return form;
    }

    @Override
    public void setForm(Form form) {
        this.form = form;
    }

}
