package ru.koptev.jse.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Form implements Serializable {

    private static final long serialVersionUID = 1L;

    private String visa;
    private String fistName;
    private String lastName;
    private int age;

    @Override
    public String toString() {
        return "Visa " + visa +
                "\r\n" + "======================" + "\r\n" +
                "Fist Name: " + fistName + "\r\n" +
                "Last name: " + lastName + "\r\n" +
                "Age: " + age;
    }

}