package ru.koptev.jse.process;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.koptev.jse.draft.Draft;
import ru.koptev.jse.form.VisaApplicationForm;
import ru.koptev.jse.sender.FormDataSender;
import ru.koptev.jse.ui.CommandInterface;

import java.util.List;

@Log4j2
@Component
public class ApplicationProcess {

    private final CommandInterface commandInterface;
    private final List<FormDataSender> formDataSenderList;
    private final ApplicationContext applicationContext;
    private final Draft draft;

    @Autowired
    public ApplicationProcess(CommandInterface commandInterface, List<FormDataSender> formDataSenderList,
                              ApplicationContext applicationContext, Draft draft) {
        this.commandInterface = commandInterface;
        this.formDataSenderList = formDataSenderList;
        this.applicationContext = applicationContext;
        this.draft = draft;
    }

    public void process() {
        VisaApplicationForm visaApplicationForm;
        String command;
        boolean flag = true;
        while (flag) {
            command = commandInterface.writeAndRead("Enter which visa you want to apply for (UK, Schengen)");
            switch (command) {
                case "Exit":
                    commandInterface.write("Closing the program");
                    flag = false;
                    break;
                case "UK":
                    visaApplicationForm = (VisaApplicationForm) applicationContext.getBean("visaUK");
                    visaApplicationForm.fillForm();
                    sendForm(visaApplicationForm);
                    break;
                case "Schengen":
                    visaApplicationForm = (VisaApplicationForm) applicationContext.getBean("visaSchengen");
                    visaApplicationForm.fillForm();
                    sendForm(visaApplicationForm);
                    break;
                case "Load":
                    visaApplicationForm = draft.load();
                    if (visaApplicationForm == null) {
                        break;
                    }
                    visaApplicationForm.fillForm();
                    sendForm(visaApplicationForm);
                    break;
                default:
                    commandInterface.write("This type of visa is not supported");
                    break;
            }
        }
    }

    private void sendForm(VisaApplicationForm visaApplicationForm) {
        if (visaApplicationForm.getForm().getFistName() == null ||
                visaApplicationForm.getForm().getLastName() == null ||
                visaApplicationForm.getForm().getAge() == 0) {
            return;
        }
        for (FormDataSender formDataSender : formDataSenderList) {
            formDataSender.send(visaApplicationForm.getFormData());
        }
    }

}
