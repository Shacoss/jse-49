package ru.koptev.jse.sender.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.koptev.jse.sender.FormDataSender;
import ru.koptev.jse.ui.CommandInterface;

@Log4j2
@Component
public class ConsoleFormDataSender implements FormDataSender {

    private final CommandInterface commandInterface;

    @Autowired
    public ConsoleFormDataSender(CommandInterface commandInterface) {
        this.commandInterface = commandInterface;
    }

    @Override
    public void send(String data) {
        commandInterface.write("Data send to console \n\r" + data);
    }

}
