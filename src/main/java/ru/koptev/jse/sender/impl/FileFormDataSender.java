package ru.koptev.jse.sender.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import ru.koptev.jse.sender.FormDataSender;
import ru.koptev.jse.ui.CommandInterface;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


@Log4j2
@Component
public class FileFormDataSender implements FormDataSender {

    private static final String STRING_SEPARATOR = "\r\n";
    private final CommandInterface commandInterface;

    public FileFormDataSender(CommandInterface commandInterface) {
        this.commandInterface = commandInterface;
    }

    @Override
    public void send(String data) {
        String fileName = data.split(STRING_SEPARATOR)[0] + "-" + System.nanoTime();
        commandInterface.write("Saving file");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName + ".txt"))) {
            bufferedWriter.write(data);
        } catch (IOException e) {
            log.error(e);
        }
    }

}
