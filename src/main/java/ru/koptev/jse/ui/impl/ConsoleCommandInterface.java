package ru.koptev.jse.ui.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import ru.koptev.jse.ui.CommandInterface;

import java.util.Scanner;

@Log4j2
@Component
@Primary
public class ConsoleCommandInterface implements CommandInterface {

    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String writeAndRead(String text) {
        log.info(text);
        return scanner.next();
    }

    @Override
    public void write(String text) {
        log.info(text);
    }

}
