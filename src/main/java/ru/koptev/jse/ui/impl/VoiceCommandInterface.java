package ru.koptev.jse.ui.impl;

import org.springframework.stereotype.Component;
import ru.koptev.jse.ui.CommandInterface;

@Component
public class VoiceCommandInterface implements CommandInterface {

    @Override
    public String writeAndRead(String text) {
        return null;
    }

    @Override
    public void write(String text) {
        throw new UnsupportedOperationException("Voice commands are not yet supported");
    }

}
